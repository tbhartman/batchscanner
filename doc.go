// Package batchscanner provides bufio.Scanner tools for scenarios where
// the standard bufio.Scanner lacks functionality, robustness, or performance:
//
// Use cases
//
// You want to support any CR/LF/CRLF line ending
// You want to return an error if mixed endings exist
// You want to support only certain endings
// You want to know what the end of line characters were in the reader
// You want the current file position
// You want to scan very large text files faster than bufio.SplitLines
//
package batchscanner

import (
	"bufio"
	"fmt"
)

// EOL identifies various types of line endings.
type EOL uint

const (
	// EmptyEOF bit is set if reader does not end in a newline.
	EmptyEOF EOL = 1

	// LF bit represents the line feed ('\n')
	LF EOL = 2

	// CR bit represents the carriage return ('\r')
	CR EOL = 4

	// CRLF bit represents the DOS line ending ('\r\n')
	CRLF EOL = 8

	// AllEOL bit represents all three line endings
	AllEOL EOL = LF | CR | CRLF

	// TotalLines denotes counting total lines via Scanner.Count
	TotalLines EOL = AllEOL | EmptyEOF
)

// ErrDisallowed is returned if options do not allow a type of line ending
// that was found.
var ErrDisallowed error

// ErrMixedEndings is returned if options do not allow mixed line endings.
var ErrMixedEndings error

func init() {
	ErrDisallowed = fmt.Errorf("reader contains disallowed line ending")
	ErrMixedEndings = fmt.Errorf("reader contains mixed line endings")
}

// Scanner replicates the bufio.Scanner interface.
type Scanner interface {
	Buffer(buf []byte, max int)
	Bytes() []byte
	Err() error
	Scan() bool
	Split(split bufio.SplitFunc)
	Text() string
}

// BatchToken is a 2-tuple of start and end positions of a token in a buffer.
type BatchToken = [2]int

// BatchTokens is an array of BatchToken.
type BatchTokens = []BatchToken

// BatchSplitFunc is analogous to bufio.SplitFunc for a BatchScanner
type BatchSplitFunc func(data []byte, atEOF bool, tokens BatchTokens, tokenIndex *int) (advance int, err error)

// A BatchScanner is similar to a bufio.Scanner but scans multiple tokens at
// once, allowing for better performance when scanning large text files.
type BatchScanner interface {
	Buffer(buf []byte, max int)
	Bytes() []byte
	Tokens() BatchTokens
	Err() error
	Scan() bool
	Split(split BatchSplitFunc)
}

// LineAnnotator supplements the Scanner interfaces by allowing access to
// additional metadata after each Scan.
type LineAnnotator interface {
	// Tell reports the buffer position at the start of the previous successful
	// scan, or the position of the current error.
	Tell() int64

	// Found reports all EOL types found, and is only guaranteed to be
	// accurate once a Scanner or BatchScanner is completed without errors.
	Found() EOL

	// Count reports the count of all the given EOLs. This is only guaranteed
	// to be accurate once a Scanner or BatchScanner is completed without
	// errors.
	Count(e EOL) int64
}

// LineSplitter provides information about reader position and end of line
// caracters for line-delimited text.  It provides either split functions
// for eiter a BatchScanner or Scanner.  Results are unpredictable if used
// in both a Scanner and BatchScanner.
type LineSplitter interface {
	LineAnnotator
	Split() bufio.SplitFunc
	BatchSplit() BatchSplitFunc
}

// A LineScanner combines a bufio.Scanner interface with LineAnnotator.
type LineScanner interface {
	Scanner
	LineAnnotator
}

// A LineBatchScanner combines a BatchScanner interface with LineAnnotator.
type LineBatchScanner interface {
	BatchScanner
	LineAnnotator
}
