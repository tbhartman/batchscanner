package batchscanner

import (
	"bufio"
)

type lineBatchSplitter func(data []byte, atEOF bool, tokens BatchTokens, tokenIndex *int) (advance int, counts eolCount, err error)

type eolCount struct {
	EmptyEOF bool
	CR       int64
	LF       int64
	CRLF     int64
}

func (e eolCount) Found() (ret EOL) {
	if e.EmptyEOF {
		ret |= EmptyEOF
	}
	if e.CR > 0 {
		ret |= CR
	}
	if e.LF > 0 {
		ret |= LF
	}
	if e.CRLF > 0 {
		ret |= CRLF
	}
	return
}
func (e *eolCount) Add(other eolCount) {
	e.EmptyEOF = e.EmptyEOF || other.EmptyEOF
	e.CR += other.CR
	e.LF += other.LF
	e.CRLF += other.CRLF
}

type lineSplitter struct {
	opt    Options
	counts eolCount

	bufpos  int64
	tellpos int64

	splitter lineBatchSplitter
}

func (ls *lineSplitter) Found() EOL {
	return ls.counts.Found()
}
func (ls *lineSplitter) Tell() int64 {
	return ls.bufpos + ls.tellpos
}
func (ls *lineSplitter) Count(e EOL) (ret int64) {
	if e&EmptyEOF > 0 && ls.counts.EmptyEOF {
		ret++
	}
	if e&CR > 0 {
		ret += ls.counts.CR
	}
	if e&LF > 0 {
		ret += ls.counts.LF
	}
	if e&CRLF > 0 {
		ret += ls.counts.CRLF
	}
	return
}
func (ls *lineSplitter) Split() bufio.SplitFunc {
	var counts eolCount
	var tokenIndex int
	var tokens BatchTokens = make(BatchTokens, 0, 1000)
	var tokenCount int
	var buffer []byte
	var toAdvance int
	var errAdvance int
	var retFunc bufio.SplitFunc
	retFunc = func(data []byte, atEOF bool) (advance int, token []byte, err error) {
		if len(tokens) > 0 && tokenIndex < len(tokens) {
			ls.tellpos = int64(tokens[tokenIndex][0])
			token = buffer[ls.tellpos:tokens[tokenIndex][1]]
			tokenIndex++
			if tokenIndex >= len(tokens) {
				advance = toAdvance
			}
			return
		}
		ls.bufpos += int64(toAdvance)
		tokenCount = 0
		toAdvance, err = runSplit(
			func(data []byte, atEOF bool, tokens [][2]int, tokenIndex *int) (advance int, err error) {
				advance, counts, err = ls.splitter(data, atEOF, tokens, tokenIndex)
				if err != nil {
					ls.tellpos += int64(advance)
					return
				}
				errAdvance, err = checkEol(data, ls.opt, ls.counts.Found(), counts.Found())
				if err != nil {
					advance = errAdvance
				} else {
					ls.counts.Add(counts)
				}
				return
			},
			data,
			atEOF,
			&tokens,
			&tokenCount,
		)

		if err != nil {
			ls.tellpos += int64(toAdvance)
			return toAdvance, nil, err
		}
		if len(tokens) > 0 {
			tokenIndex = 0
			buffer = data[:toAdvance]
			return retFunc(data, atEOF)
		}
		if atEOF {
			ls.tellpos = 0
		}
		// if no tokens found, request more
		return toAdvance, nil, err
	}
	return retFunc
}

func (ls *lineSplitter) BatchSplit() BatchSplitFunc {
	var counts eolCount
	var toAdvance int
	var pos int
	return func(data []byte, atEOF bool, tokens BatchTokens, tokenIndex *int) (advance int, err error) {
		ls.bufpos += int64(toAdvance)
		advance, counts, err = ls.splitter(data, atEOF, tokens, tokenIndex)
		if err != nil {
			ls.bufpos += int64(advance)
			return
		}
		pos = 0
		pos, err = checkEol(data, ls.opt, ls.counts.Found(), counts.Found())
		if err != nil {
			ls.bufpos += int64(pos)
			return pos, err
		}
		ls.counts.Add(counts)
		toAdvance = advance
		return
	}
}
