package batchscanner

import (
	"bytes"
	"math"
	"math/bits"
)

// below are pure functions for use throughout batchscanner

// resetBuffer moves any bytes past position of b to the front of
// b and resets the length accordingly
func resetBuffer(position int, b *[]byte) {
	switch {
	case position >= len(*b):
		*b = (*b)[:0]
	case position > 0:
		copy(*b, (*b)[position:])
		*b = (*b)[:len(*b)-position]
	}
}

// getRemainingCapacityPow2 returns the unused portion of b, to the next lower
// power of two.  Example, if 1030 == cap(b) - len(b), this returns 1024.
func getRemainingCapacityPow2(b []byte) int {
	return int(math.Pow(2, math.Floor(math.Log2(float64(cap(b)-len(b))))))
}

// runSplit runs a BatchSplitFunc, getting tokens and catching overflows of
// the tokens slice, enlarging the slice.  tokenCount must be passed in, and
// must be zero at the start of each call.  It is placed out of here to
// reduce allocation count.
func runSplit(f BatchSplitFunc, data []byte, atEOF bool, tokens *BatchTokens, tokenCount *int) (advance int, err error) {
	// recover if tokenCount >= len(tokens)
	defer func() {
		if r := recover(); r != nil {
			if tokens == nil {
				*tokens = make(BatchTokens, 100)
				advance, err = runSplit(f, data, atEOF, tokens, tokenCount)
				return
			}
			if *tokenCount >= len(*tokens) && len(*tokens) < len(data) {
				// increase lines and try again...could recurse, but panic if
				// len(lines) gets above len(buffer)!

				if *tokens == nil {
					// um, yeah, that would be a problem
					*tokens = make(BatchTokens, 100)
				} else {
					*tokens = make(BatchTokens, cap(*tokens)*2)
				}
				*tokenCount = 0
				advance, err = runSplit(f, data, atEOF, tokens, tokenCount)
				return
			}
			panic(r)
		}
	}()

	*tokens = (*tokens)[:cap(*tokens)]
	advance, err = f(data, atEOF, *tokens, tokenCount)

	*tokens = (*tokens)[:*tokenCount]
	return
}

func findEol(data []byte, e EOL) int {
	switch e {
	case CR:
		for i := 0; i < len(data); i++ {
			if data[i] == '\r' && (i+1 >= len(data) || data[i+1] != '\n') {
				return i
			}
		}
	case LF:
		for i := 0; i < len(data); i++ {
			if data[i] == '\n' && (i == 0 || data[i-1] != '\r') {
				return i
			}
		}
	case CRLF:
		return bytes.Index(data, []byte("\r\n"))
	}
	return -1
}

// checkEol checks current and next found EOL against opt.  If an error is found,
// checkEol searches data for the error and returns the position of the error
// and the error, otherwise it returns 0, nil.
func checkEol(data []byte, opt Options, current EOL, next EOL) (position int, err error) {

	found := current | next&AllEOL
	unallowed := next &^ opt.AllowEndings
	var isMixed = bits.OnesCount(uint(found)) > 1
	var isUnallowed = next&^opt.AllowEndings > 0

	// check for not allowed first
	if opt.CheckAllowed && isUnallowed {
		err = ErrDisallowed
		switch {
		case unallowed&CRLF != 0:
			position = findEol(data, CRLF)
		case unallowed&CR != 0:
			position = findEol(data, CR)
		case unallowed&LF != 0:
			position = findEol(data, LF)
		case unallowed&EmptyEOF != 0:
			position = len(data)
		default:
			panic("Invalid options")
		}
		return
	}

	// then check for mixed
	if opt.CheckMixed && isMixed {
		err = ErrMixedEndings
		switch {
		case current&CRLF == 0 && next&CRLF != 0:
			position = findEol(data, CRLF)
		case current&CR == 0 && next&CR != 0:
			position = findEol(data, CR)
		default:
			position = findEol(data, LF)
		}
		return
	}
	return 0, nil
}
