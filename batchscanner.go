package batchscanner

import (
	"bufio"
	"fmt"
	"io"
)

func newBatchScanner(r io.Reader) *batchScanner {
	return &batchScanner{
		r: r,
	}
}

type batchScanner struct {
	r io.Reader

	tokens     BatchTokens
	tokenCount int

	// buffer to store reads
	buffer        []byte
	maxBufferSize int
	toAdvance     int

	// hold any error to send out
	e error

	started  bool
	finished bool

	splitFunc BatchSplitFunc
}

func (b *batchScanner) Bytes() []byte {
	return b.buffer
}

func (b *batchScanner) Tokens() BatchTokens {
	return b.tokens
}

func (b *batchScanner) Err() error {
	return b.e
}

func (b *batchScanner) Split(f BatchSplitFunc) {
	if b.started {
		panic("may not set Split after scan started")
	}
	b.splitFunc = f
}

func (b *batchScanner) Buffer(data []byte, m int) {
	if b.started {
		panic("may not set Buffer after scan started")
	}
	if m < cap(data) {
		m = cap(data)
	}
	b.buffer = data[:0]
	b.maxBufferSize = m
	// create an array for tokens; assume a typical size of token
	// this will be adjusted on the fly in runSplit, but this gives something
	// reasonable.  Example, if splitting text lines, "m/10" means each line
	// (token) would have 10 characters.
	b.tokens = make(BatchTokens, 0, m/30)
	// b.tokens = make(BatchTokens, 0, 1000)
}

// setFinished marks the scan as complete, with error if e is not nil.  This
// always returns false
func (b *batchScanner) setFinished(e error) bool {
	b.e = e
	b.finished = true
	b.tokens = b.tokens[:0]
	return false
}

func (b *batchScanner) Scan() bool {
	if b.finished {
		return false
	}

	if !b.started {
		if b.buffer == nil {
			b.Buffer(make([]byte, bufio.MaxScanTokenSize), bufio.MaxScanTokenSize)
			// b.Buffer(make([]byte, 32768), 32768)
		}
		if b.splitFunc == nil {
			s := NewLineSplitter(OptDefault)
			b.splitFunc = s.BatchSplit()
		}
		b.started = true
	}

	resetBuffer(b.toAdvance, &b.buffer)

	// get more data (to a length of 2^*)
	toRead := getRemainingCapacityPow2(b.buffer)
	if toRead < 1024 && cap(b.buffer) < b.maxBufferSize {
		// increase buffer size
		newBuffer := make([]byte, b.maxBufferSize)
		copy(newBuffer, b.buffer)
		b.buffer = newBuffer
		toRead = getRemainingCapacityPow2(b.buffer)
	}
	if toRead < 2 {
		return b.setFinished(fmt.Errorf("Out of buffer space"))
	}

	n, err := b.r.Read(b.buffer[len(b.buffer) : len(b.buffer)+toRead])
	b.buffer = b.buffer[:len(b.buffer)+n]

	// find all tokens in buffer
	var advance int
	b.tokenCount = 0
	advance, err = runSplit(b.splitFunc, b.buffer, err == io.EOF, &b.tokens, &b.tokenCount)
	b.toAdvance = advance

	if err != nil {
		return b.setFinished(err)
	}
	if len(b.tokens) == 0 {
		if n == 0 {
			// reached the end and no more tokens found...finish
			return b.setFinished(nil)
		}
		// get more data and try to find tokens again
		return b.Scan()
	}

	return true
}
