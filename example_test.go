package batchscanner_test

import (
	"fmt"
	"strings"

	"gitlab.com/tbhartman/batchscanner"
)

func ExampleLineScanner() {
	reader := strings.NewReader(
		"An example reader.\n" +
			"With all EOL types.\r" +
			"Is scanned correctly.\r\n" +
			"And provides counts of EOLs.\r" +
			"Even the lack of EOF is provided.",
	)

	scanner := batchscanner.NewLineScanner(reader, batchscanner.OptParseAny)

	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	fmt.Println("")
	fmt.Printf("Total lines: %d\n", scanner.Count(batchscanner.TotalLines))
	fmt.Printf("      LF   : %d\n", scanner.Count(batchscanner.LF))
	fmt.Printf("      CR   : %d\n", scanner.Count(batchscanner.CR))
	fmt.Printf("      CRLF : %d\n\n", scanner.Count(batchscanner.CRLF))

	hasEOF := scanner.Found()&batchscanner.EmptyEOF == 0
	fmt.Printf("Has EOF    : %v\n", hasEOF)

	// Output:
	// An example reader.
	// With all EOL types.
	// Is scanned correctly.
	// And provides counts of EOLs.
	// Even the lack of EOF is provided.
	//
	// Total lines: 5
	//       LF   : 1
	//       CR   : 2
	//       CRLF : 1
	//
	// Has EOF    : false
}
