package batchscanner_test

import (
	"bufio"
	"bytes"
	"io"
	"math/rand"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	bs "gitlab.com/tbhartman/batchscanner"
)

type infReader struct {
	data  []byte
	index int
}

func (i *infReader) Reset() {
	i.index = 0
}
func (i *infReader) Read(b []byte) (int, error) {
	var index int
	for index < len(b) {
		togoInBuffer := len(b) - index
		lineRemainder := i.data[i.index:]
		togoInLine := len(lineRemainder)
		if togoInBuffer <= togoInLine {
			// just copy rest of line into buffer
			copied := copy(b[index:], lineRemainder)
			i.index += copied
			break
		}
		// rest of data fits into buffer
		copied := copy(b[index:index+togoInLine], lineRemainder)
		index += copied
		i.index = 0
	}
	return len(b), nil
}

func TestInfReader(t *testing.T) {
	r := &infReader{
		data: []byte("abc\r\ndef\r\n"),
	}
	b := make([]byte, 2)
	var count int
	var err error

	count, err = r.Read(b)
	assert.Equal(t, 2, count)
	assert.NoError(t, err)
	assert.EqualValues(t, "ab", string(b))

	count, err = r.Read(b)
	assert.Equal(t, 2, count)
	assert.NoError(t, err)
	assert.EqualValues(t, "c\r", string(b))

	b = make([]byte, 9)
	count, err = r.Read(b)
	assert.Equal(t, 9, count)
	assert.NoError(t, err)
	assert.EqualValues(t, "\ndef\r\nabc", string(b))

	count, err = r.Read(b)
	assert.Equal(t, 9, count)
	assert.NoError(t, err)
	assert.EqualValues(t, "\r\ndef\r\nab", string(b))

	count, err = r.Read(b)
	assert.Equal(t, 9, count)
	assert.NoError(t, err)
	assert.EqualValues(t, "c\r\ndef\r\na", string(b))

	count, err = r.Read(b)
	assert.Equal(t, 9, count)
	assert.NoError(t, err)
	assert.EqualValues(t, "bc\r\ndef\r\n", string(b))
}

func BenchmarkScanner(b *testing.B) {
	// a new EOL to mark mixed
	var MixedEOL bs.EOL = 512
	// create some test files
	type makerStruct struct {
		f    *os.File
		name string
	}
	filedata := map[bs.EOL]*makerStruct{
		bs.CRLF:  {nil, "benchmark.crlf.txt"},
		bs.CR:    {nil, "benchmark.cr.txt"},
		bs.LF:    {nil, "benchmark.lf.txt"},
		MixedEOL: {nil, "benchmark.mixed.txt"},
	}
	var allFilesOk bool = true
	var linesWritten int64
	for eol, fd := range filedata {
		g, err := os.Open(fd.name)
		if err == nil {
			if eol == bs.LF {
				s := bufio.NewScanner(g)
				for s.Scan() {
					linesWritten++
				}
			}
			continue
		}
		allFilesOk = false
	}
	const targetBytes int64 = 2e7
	if !allFilesOk {
		linesWritten = 0
		for _, fd := range filedata {
			if fd.f != nil {
				fd.f.Close()
			}
			g, err := os.Create(fd.name)
			if err != nil {
				panic(err)
			}
			fd.f = g
		}
		const alpha string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234536789 `!@#$%^&*()_+-={}|[]\\:\";'<>?,./"
		var maxLen int = 80
		var minLen int = 10
		var lineLen int
		var bytesWritten int64
		for bytesWritten < targetBytes {
			var line string
			lineLen = minLen + rand.Intn(maxLen-minLen)
			for j := 0; j < lineLen; j++ {
				line += string(alpha[rand.Int63()%int64(len(alpha))])
			}
			linesWritten++
			for ending, fd := range filedata {
				switch ending {
				case bs.CR:
					fd.f.WriteString(line + "\r")
				case bs.LF:
					fd.f.WriteString(line + "\n")
				case bs.CRLF:
					fd.f.WriteString(line[:len(line)-1] + "\r\n")
				default:
					switch rand.Intn(2) {
					case 0:
						fd.f.WriteString(line + "\r")
					case 1:
						fd.f.WriteString(line + "\n")
					default:
						fd.f.WriteString(line[:len(line)-1] + "\r\n")
					}
				}
			}
			bytesWritten += int64(len(line))
		}
		for _, fd := range filedata {
			fd.f.Close()
		}
	}
	for _, fd := range filedata {
		g, err := os.Open(fd.name)
		if err != nil {
			panic(err)
		}
		fd.f = g
	}
	var crlfFile *os.File = filedata[bs.CRLF].f
	var crFile *os.File = filedata[bs.CR].f
	var lfFile *os.File = filedata[bs.LF].f
	var mixedFile *os.File = filedata[MixedEOL].f
	defer func() {
		crlfFile.Close()
		crFile.Close()
		lfFile.Close()
		mixedFile.Close()
	}()

	type scannerI interface {
		Scan() bool
		Bytes() []byte
	}
	type liner interface {
		scannerI
		Count(e bs.EOL) int64
	}

	// for each ending, plus mixed
	const targetBufferSize int = 32768
	runAll := bs.AllEOL | MixedEOL
	for _, d := range []struct {
		name string
		f    func(r io.Reader) scannerI
		bs.EOL
	}{
		{"bufio.Scanner", func(r io.Reader) scannerI { return bufio.NewScanner(bufio.NewReaderSize(r, targetBufferSize)) }, bs.CRLF | bs.LF},
		{"WellFormed", func(r io.Reader) scannerI {
			return bs.NewLineBatchScanner(bufio.NewReaderSize(r, targetBufferSize), bs.OptWellFormed)
		}, bs.CRLF | bs.LF},
		{"WellFormedLine", func(r io.Reader) scannerI {
			return bs.NewLineScanner(bufio.NewReaderSize(r, targetBufferSize), bs.OptWellFormed)
		}, bs.CRLF | bs.LF},
		{"UnixDos", func(r io.Reader) scannerI {
			return bs.NewLineBatchScanner(bufio.NewReaderSize(r, targetBufferSize), bs.OptBufio)
		}, bs.CRLF | bs.LF},
		{"UnixDosLine", func(r io.Reader) scannerI {
			return bs.NewLineScanner(bufio.NewReaderSize(r, targetBufferSize), bs.OptBufio)
		}, bs.CRLF | bs.LF},
		{"Any", func(r io.Reader) scannerI { return bs.NewLineBatchScanner(r, bs.OptParseAny) }, runAll},
		{"AnyLine", func(r io.Reader) scannerI {
			return bs.NewLineScanner(bufio.NewReaderSize(r, targetBufferSize), bs.OptParseAny)
		}, runAll},
		{"LFOnly", func(r io.Reader) scannerI { return bs.NewLineBatchScanner(r, bs.Options{AllowEndings: bs.LF}) }, bs.LF},
		{"CROnly", func(r io.Reader) scannerI { return bs.NewLineBatchScanner(r, bs.Options{AllowEndings: bs.CR}) }, bs.CR},
		{"CRLFOnly", func(r io.Reader) scannerI { return bs.NewLineBatchScanner(r, bs.Options{AllowEndings: bs.CRLF}) }, bs.CRLF},
	} {
		b.Run(d.name, func(b *testing.B) {
			for _, eol := range []bs.EOL{bs.CR, bs.LF, bs.CRLF, MixedEOL} {
				if d.EOL&eol > 0 {
					var f *os.File
					var name string
					switch eol {
					case bs.CR:
						f = crFile
						name = "CR"
					case bs.LF:
						f = lfFile
						name = "LF"
					case bs.CRLF:
						f = crlfFile
						name = "CRLF"
					case MixedEOL:
						f = mixedFile
						name = "Mixed"
					default:
						panic(eol)
					}
					var s scannerI
					b.Run(name, func(b *testing.B) {
						var lines int64
						for i := 0; i < b.N; i++ {
							lines = 0
							f.Seek(0, io.SeekStart)
							s = d.f(f)
							// try to set BatchScanner buffer
							if batchScanner, ok := s.(bs.BatchScanner); ok {
								batchScanner.Buffer(make([]byte, 0, 16384+400), 0)
							}
							for s.Scan() {
								lines++
								bytes.Count(s.Bytes(), []byte("a"))
								// bytes.Count(s.Bytes(), []byte("b"))
								// bytes.Count(s.Bytes(), []byte("c"))
								// bytes.Count(s.Bytes(), []byte("d"))
							}
							ls, ok := s.(bs.LineAnnotator)
							if ok {
								assert.EqualValues(b, linesWritten, ls.Count(bs.TotalLines))
							} else {
								assert.EqualValues(b, linesWritten, lines)
							}
						}
						b.ReportAllocs()
					})
				}
			}
		})
	}
}
