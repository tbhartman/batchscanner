Batch Scanner
=============

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/tbhartman/batchscanner.svg)](https://pkg.go.dev/gitlab.com/tbhartman/batchscanner)
[![pipeline status](https://gitlab.com/tbhartman/batchscanner/badges/master/pipeline.svg)](https://gitlab.com/tbhartman/batchscanner/-/commits/master)
[![coverage report](https://gitlab.com/tbhartman/batchscanner/badges/master/coverage.svg)](https://gitlab.com/tbhartman/batchscanner/-/commits/master)
[![goreport](https://goreportcard.com/badge/gitlab.com/tbhartman/batchscanner)](https://goreportcard.com/report/gitlab.com/tbhartman/batchscanner)

*a go batch Scanner*

This package implements tools similar to [`bufio.Scanner`](https://golang.org/pkg/bufio/#Scanner)
that allow files with any type of line ending (DOS/UNIX/MAC), or allow scanning
of large files **more efficiently**.  An example run of the benchmark:

    go test -bench "/(bufio|Formed$)/^LF" -benchtime 1x  -run=^$

results in:

    goos: windows
    goarch: amd64
    pkg: gitlab.com/tbhartman/batchscanner
    BenchmarkScanner/bufio.Scanner/LF-8                    1         205002200 ns/  op           37328 B/op          8 allocs/op
    BenchmarkScanner/WellFormed/LF-8                       1         115994500 ns/  op           60000 B/op         15 allocs/op
    PASS
    ok      gitlab.com/tbhartman/batchscanner       1.099s

In that benchmark, a 20MB file is read once, counting the number 
of lines in the file and running several `bytes.Count` to represent
some non-tivial work performed on the data. The `bufio.Scanner`
benchmark uses a `bufio.Scanner` with buffered reader size of `32kB`,
scanning each line sequentially.  The `WellFormed` benchmark scans the
same file, but each `Scan` returns a batch of tokens (instead of a single token; see docs), and the file line endings are validated to be
either all CRLF or all LF.  In this case, the `WellFormed` benchmark
ran more than 40% faster that `bufio.Scanner`, using about 40% more
memory.

To parse a file and ensure only CRLF or only LF line endings:

```go
import (
    bs "gitlab.com/tbhartman/batchscanner"
)

func main() {
    f, _ := os.Open("filename")
    scanner := bs.NewLineBatchScanner(f, bs.OptWellFormed)

    var buffer []byte
    var line []byte
    for scanner.Scan() {
        buffer = scanner.Bytes()
        for _, tokenPos := range scanner.Tokens() {
            line = buffer[tokenPos[0]:tokenPos[1]]
            ...
        }
    }
    var lineCount int64 = scanner.Count(bs.TotalLines)
}
```

To parse a file with any of the three (or mixed!) line endings,
replace `bs.OptWellFormed` with `bs.OptParseAny`.

To get an interface the same as `bufio.Scanner`, use `bs.NewLineScanner`:

```go
import (
    bs "gitlab.com/tbhartman/batchscanner"
)

func main() {
    f, _ := os.Open("filename")
    scanner := bs.NewLineScanner(f, bs.OptWellFormed)

    var buffer []byte
    var line []byte
    var lineCount int64
    for scanner.Scan() {
        lineCount++
        line = scanner.Bytes()
        ...
    }
}
```

See the [examples](https://pkg.go.dev/gitlab.com/tbhartman/batchscanner#pkg-examples)
