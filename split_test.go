package batchscanner_test

import (
	"io"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	bs "gitlab.com/tbhartman/batchscanner"
)

func TestBasic(t *testing.T) {
	str := "a\nb\rc\r\nd\r"
	s := bs.NewLineScanner(strings.NewReader(str), bs.OptParseAny)
	assert.True(t, s.Scan())
	assert.Equal(t, "a", s.Text())
	assert.True(t, s.Scan())
	assert.Equal(t, "b", s.Text())
	assert.True(t, s.Scan())
	assert.Equal(t, "c", s.Text())
	assert.True(t, s.Scan())
	assert.Equal(t, "d", s.Text())
	assert.False(t, s.Scan())
	assert.EqualValues(t, 2, s.Count(bs.CR))
	assert.EqualValues(t, 1, s.Count(bs.CRLF))
	assert.EqualValues(t, 1, s.Count(bs.LF))
	assert.EqualValues(t, 4, s.Count(bs.TotalLines))
	assert.EqualValues(t, 4, s.Count(bs.AllEOL))
	assert.Equal(t, bs.AllEOL, s.Found())
}

func TestTell(t *testing.T) {
	str := "a\nb\rc\r\nd\n"
	s := bs.NewLineScanner(strings.NewReader(str), bs.OptParseAny)
	assert.True(t, s.Scan())
	assert.EqualValues(t, 0, s.Tell())
	assert.True(t, s.Scan())
	assert.EqualValues(t, 2, s.Tell())
	assert.True(t, s.Scan())
	assert.EqualValues(t, 4, s.Tell())
	assert.True(t, s.Scan())
	assert.EqualValues(t, 7, s.Tell())
	assert.False(t, s.Scan())
	assert.EqualValues(t, 9, s.Tell())
}

func TestUniqueSearch(t *testing.T) {
	t.Run("CR", func(t *testing.T) {
		s := bs.NewLineScanner(
			&smallReader{s: "a\rb\rc\rd"},
			bs.Options{AllowEndings: bs.CR},
		)
		assert.True(t, s.Scan())
		assert.EqualValues(t, "a", s.Text())
		assert.True(t, s.Scan())
		assert.EqualValues(t, "b", s.Text())
		assert.True(t, s.Scan())
		assert.EqualValues(t, "c", s.Text())
		assert.True(t, s.Scan())
		assert.EqualValues(t, "d", s.Text())
		assert.False(t, s.Scan())
		assert.Greater(t, bs.EmptyEOF&s.Found(), bs.EOL(0))
		assert.NoError(t, s.Err())
	})
	t.Run("CRLF", func(t *testing.T) {
		s := bs.NewLineScanner(
			&smallReader{s: "a\r\nb\r\nc\r\nd"},
			bs.Options{AllowEndings: bs.CRLF},
		)
		assert.True(t, s.Scan())
		assert.EqualValues(t, "a", s.Text())
		assert.True(t, s.Scan())
		assert.EqualValues(t, "b", s.Text())
		assert.True(t, s.Scan())
		assert.EqualValues(t, "c", s.Text())
		assert.True(t, s.Scan())
		assert.EqualValues(t, "d", s.Text())
		assert.False(t, s.Scan())
		assert.Greater(t, bs.EmptyEOF&s.Found(), bs.EOL(0))
	})
	t.Run("LF", func(t *testing.T) {
		s := bs.NewLineScanner(
			&smallReader{s: "a\nb\nc\nd"},
			bs.Options{AllowEndings: bs.LF},
		)
		assert.True(t, s.Scan())
		assert.EqualValues(t, "a", s.Text())
		assert.True(t, s.Scan())
		assert.EqualValues(t, "b", s.Text())
		assert.True(t, s.Scan())
		assert.EqualValues(t, "c", s.Text())
		assert.True(t, s.Scan())
		assert.EqualValues(t, "d", s.Text())
		assert.False(t, s.Scan())
		assert.Greater(t, bs.EmptyEOF&s.Found(), bs.EOL(0))
	})
}

func TestWellFormed(t *testing.T) {
	// no eof
	s := bs.NewLineScanner(
		&smallReader{s: "a\nb\nc\nd"},
		bs.OptWellFormed,
	)
	for s.Scan() {
	}
	assert.ErrorIs(t, s.Err(), bs.ErrDisallowed)

	s = bs.NewLineScanner(
		&smallReader{s: "a\nb\nc\nd\n"},
		bs.OptWellFormed,
	)
	for s.Scan() {
	}
	assert.NoError(t, s.Err())

	s = bs.NewLineScanner(
		&smallReader{s: "a\r\nb\r\nc\r\nd\r\n"},
		bs.OptWellFormed,
	)
	for s.Scan() {
	}
	assert.NoError(t, s.Err())

	// disallowed
	s = bs.NewLineScanner(
		&smallReader{s: "a\r\nb\r\nc\r\nd\r"},
		bs.OptWellFormed,
	)
	for s.Scan() {
	}
	assert.ErrorIs(t, s.Err(), bs.ErrDisallowed)

	// mixed
	s = bs.NewLineScanner(
		&smallReader{s: "a\r\nb\r\nc\r\nd\n"},
		bs.OptWellFormed,
	)
	for s.Scan() {
	}
	assert.ErrorIs(t, s.Err(), bs.ErrMixedEndings)
}

func TestNoEOF(t *testing.T) {
	str := "d"
	s := bs.NewLineScanner(strings.NewReader(str), bs.OptParseAny)
	for s.Scan() {
	}
	assert.Less(t, bs.EOL(0), s.Found()&bs.EmptyEOF)
}

type smallReader struct {
	s   string
	pos int64
}

func (s *smallReader) Read(a []byte) (int, error) {
	if len(a) == 0 {
		return 0, nil
	}
	if len(s.s) == 0 {
		return 0, io.EOF
	}
	a[0] = s.s[0]
	s.s = s.s[1:]
	s.pos++
	return 1, nil
}
func (s *smallReader) Seek(at int64, whence int) (int64, error) {
	return s.pos, nil
}

// func TestSplitFunc(t *testing.T) {
// 	s := bufio.NewScanner(strings.NewReader("a\rb\n"))
// 	s.Split(bs.NewSplitFunc(bs.OptParseAny))
// 	s.Scan()
// 	assert.Equal(t, "a", s.Text())
// 	s.Scan()
// 	assert.Equal(t, "b", s.Text())
// }

func TestExitImmediately(t *testing.T) {
	var r io.ReadSeeker
	r = &smallReader{s: "a\r\nb\nlots of data after that"}
	s := bs.NewLineScanner(r, bs.Options{CheckMixed: true})
	for s.Scan() {
	}
	assert.Equal(t, bs.ErrMixedEndings, s.Err())
	pos, _ := r.Seek(0, io.SeekCurrent)
	assert.EqualValues(t, 5, pos)
	assert.EqualValues(t, 4, s.Tell())

	r = &smallReader{s: "a\rb\nlots of data after that"}
	s = bs.NewLineScanner(r, bs.Options{
		AllowEndings: bs.LF,
		CheckAllowed: true,
	})
	for s.Scan() {
	}
	assert.Equal(t, bs.ErrDisallowed, s.Err())
	pos, _ = r.Seek(0, io.SeekCurrent)
	assert.EqualValues(t, 3, pos)
	assert.EqualValues(t, 1, s.Tell())
}

func TestEmptyEOF(t *testing.T) {
	s := bs.NewLineScanner(
		strings.NewReader("a\nb\nc"),
		bs.Options{AllowEndings: bs.LF | bs.EmptyEOF, CheckAllowed: true},
	)
	assert.True(t, s.Scan())
	assert.Equal(t, "a", s.Text())
	assert.True(t, s.Scan())
	assert.Equal(t, "b", s.Text())
	assert.True(t, s.Scan())
	assert.Equal(t, "c", s.Text())
	assert.False(t, s.Scan())
	assert.True(t, s.Found()&bs.EmptyEOF > 0)
	assert.NoError(t, s.Err())
}
