package batchscanner

import (
	"bufio"
	"io"
	"reflect"
)

// New returns a BatchScanner using OptDefault
func New(r io.Reader) BatchScanner {
	return NewOptions(r, OptDefault)
}

// NewOptions returns a BatchScanner, using opt
func NewOptions(r io.Reader, opt Options) BatchScanner {
	return NewLineBatchScanner(r, opt)
}

type lineScanner struct {
	Scanner
	LineSplitter
}

func (ls *lineScanner) Split(f bufio.SplitFunc) {
	ls.Scanner.Split(f)
}

// NewLineScanner returns a new LineScanner using opt
func NewLineScanner(r io.Reader, opt Options) LineScanner {
	ret := &lineScanner{
		Scanner:      bufio.NewScanner(r),
		LineSplitter: NewLineSplitter(opt),
	}
	ret.Scanner.Split(ret.LineSplitter.Split())
	return ret
}

type lineBatchScanner struct {
	BatchScanner
	LineSplitter
}

func (lbs *lineBatchScanner) Split(f BatchSplitFunc) {
	lbs.BatchScanner.Split(f)
}

// NewLineBatchScanner returns a new LineBatchScanner using opt
func NewLineBatchScanner(r io.Reader, opt Options) LineBatchScanner {
	ret := &lineBatchScanner{
		BatchScanner: newBatchScanner(r),
		LineSplitter: NewLineSplitter(opt),
	}
	ret.BatchScanner.Split(ret.LineSplitter.BatchSplit())
	return ret
}

// NewLineSplitter returns a new LineSplitter using opt
func NewLineSplitter(opt Options) LineSplitter {
	if opt.AllowEndings == 0 {
		opt.AllowEndings = AllEOL | EmptyEOF
	}
	ls := &lineSplitter{
		opt: opt,
	}
	// here is my chance to optimize
	var splitter lineBatchSplitter
	switch {
	case reflect.DeepEqual(opt, OptWellFormed):
		splitter = wellFormedSplitter
		// splitter = anySplitter
	case reflect.DeepEqual(opt, OptBufio):
		// splitter = wellFormedSplitter
		splitter = quickSplitter
	case !opt.CheckAllowed && !opt.CheckMixed:
		switch {
		case opt.AllowEndings&^(LF|EmptyEOF) == 0:
			splitter = lfSplitter
		case opt.AllowEndings&^(CRLF|EmptyEOF) == 0:
			splitter = crlfSplitter
		case opt.AllowEndings&^(CR|EmptyEOF) == 0:
			splitter = crSplitter
		default:
			splitter = anySplitter
		}
	default:
		// no special case found, look for all endings
		splitter = anySplitter
	}
	ls.splitter = splitter
	return ls
}
