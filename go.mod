module gitlab.com/tbhartman/batchscanner

go 1.13

require (
	github.com/stretchr/testify v1.7.0
	gitlab.com/tbhartman/alleolscanner v1.0.2 // indirect
)
