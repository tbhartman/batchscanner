package batchscanner

import (
	"bytes"
)

func clearEndingCr(data *[]byte, skip bool) {
	if skip {
		return
	}
	if len(*data) > 0 && (*data)[len(*data)-1] == '\r' {
		*data = (*data)[:len(*data)-1]
	}
}

func checkEOF(data []byte, tokens BatchTokens, tokenIndex *int, advance *int, check EOL) bool {
	var found bool
	var length int
	if len(data) > 0 {
		switch check {
		case CR:
			if data[len(data)-1] == '\r' {
				found = true
				length = 1
			}
		case LF:
			if data[len(data)-1] == '\r' {
				found = true
				length = 1
			}
		case EmptyEOF:
			if data[len(data)-1] != '\r' && data[len(data)-1] != '\n' {
				found = true
				length = 0
			}
		default:
			panic("not supported")
		}
		if found {
			tokens[*tokenIndex][0] = *advance
			tokens[*tokenIndex][1] = len(data) - length
			*advance = len(data)
			*tokenIndex++
		}
	}
	return found
}

func wellFormedSplitter(data []byte, atEOF bool, tokens BatchTokens, tokenIndex *int) (advance int, counts eolCount, err error) {
	clearEndingCr(&data, atEOF)

	var crlfcount int
	var lfcount int
	var crcount int

	// get all newlines
	var i int
	var index int
	if len(data) > 0 && data[i] == '\n' {
		tokens[*tokenIndex][0] = i
		tokens[*tokenIndex][1] = i
		*tokenIndex++
		lfcount++
		advance++
	}
	i++
	for i < len(data) {
		index = bytes.IndexByte(data[i:], '\n')
		if index < 0 {
			break
		}
		tokens[*tokenIndex][0] = advance
		if data[i+index-1] == '\r' {
			crlfcount++
			tokens[*tokenIndex][1] = i + index - 1
		} else {
			lfcount++
			tokens[*tokenIndex][1] = i + index
		}
		*tokenIndex++
		advance = i + index + 1
		i += index + 1
	}

	crcount = bytes.Count(data, []byte("\r"))
	counts.CR = int64(crcount - crlfcount)
	counts.LF = int64(lfcount)
	counts.CRLF = int64(crlfcount)

	if atEOF {
		if checkEOF(data, tokens, tokenIndex, &advance, CR) {
			counts.CR++
		}
		if checkEOF(data, tokens, tokenIndex, &advance, EmptyEOF) {
			counts.EmptyEOF = true
		}
	}

	return
}

func anySplitter(data []byte, atEOF bool, tokens BatchTokens, tokenIndex *int) (advance int, counts eolCount, err error) {
	clearEndingCr(&data, atEOF)

	var crlfcount int
	var lfcount int
	var crcount int

	// need to find all types of newlines
	// buffer will not end with CR unless this is at EOF
	var i int
	for ; i < len(data); i++ {
		if data[i] == '\n' {
			lfcount++
			tokens[*tokenIndex][0] = advance
			tokens[*tokenIndex][1] = i
			*tokenIndex++
			advance = i + 1
		} else if data[i] == '\r' {
			if i+1 < len(data) && data[i+1] == '\n' {
				crlfcount++
				tokens[*tokenIndex][0] = advance
				tokens[*tokenIndex][1] = i
				*tokenIndex++
				advance = i + 2
				i++
			} else {
				crcount++
				tokens[*tokenIndex][0] = advance
				tokens[*tokenIndex][1] = i
				*tokenIndex++
				advance = i + 1
			}
		}
	}

	counts.CR = int64(crcount)
	counts.LF = int64(lfcount)
	counts.CRLF = int64(crlfcount)

	if atEOF {
		if checkEOF(data, tokens, tokenIndex, &advance, EmptyEOF) {
			counts.EmptyEOF = true
		}
	}

	return
}

// quickSplitter only looks for CRLF/LF and does not worry about CR
func quickSplitter(data []byte, atEOF bool, tokens BatchTokens, tokenIndex *int) (advance int, counts eolCount, err error) {
	var crlfcount int
	var lfcount int

	var i int
	var index int
	if len(data) > 0 && data[i] == '\n' {
		tokens[*tokenIndex][0] = i
		tokens[*tokenIndex][1] = i
		*tokenIndex++
		lfcount++
		advance++
	}
	i++
	for i < len(data) {
		index = bytes.IndexByte(data[i:], '\n')
		if index < 0 {
			break
		}
		tokens[*tokenIndex][0] = advance
		if data[i+index-1] == '\r' {
			crlfcount++
			tokens[*tokenIndex][1] = i + index - 1
		} else {
			lfcount++
			tokens[*tokenIndex][1] = i + index
		}
		*tokenIndex++
		advance = i + index + 1
		i += index + 1
	}

	counts.LF = int64(lfcount)
	counts.CRLF = int64(crlfcount)

	if atEOF {
		if checkEOF(data, tokens, tokenIndex, &advance, EmptyEOF) {
			counts.EmptyEOF = true
		}
	}

	return
}

func lfSplitter(data []byte, atEOF bool, tokens BatchTokens, tokenIndex *int) (advance int, counts eolCount, err error) {
	var count int

	var i int
	for ; i < len(data); i++ {
		if data[i] == '\n' {
			count++
			tokens[*tokenIndex][0] = advance
			tokens[*tokenIndex][1] = i
			*tokenIndex++
			advance = i + 1
		}
	}

	counts.LF = int64(count)

	if atEOF {
		if checkEOF(data, tokens, tokenIndex, &advance, EmptyEOF) {
			counts.EmptyEOF = true
		}
	}

	return
}

func crlfSplitter(data []byte, atEOF bool, tokens BatchTokens, tokenIndex *int) (advance int, counts eolCount, err error) {
	var count int

	var i int
	for ; i < len(data); i++ {
		if data[i] == '\n' {
			count++
			tokens[*tokenIndex][0] = advance
			tokens[*tokenIndex][1] = i - 1
			*tokenIndex++
			advance = i + 1
		}
	}

	counts.CRLF = int64(count)

	if atEOF {
		if checkEOF(data, tokens, tokenIndex, &advance, EmptyEOF) {
			counts.EmptyEOF = true
		}
	}

	return
}

func crSplitter(data []byte, atEOF bool, tokens BatchTokens, tokenIndex *int) (advance int, counts eolCount, err error) {
	var count int

	var i int
	for ; i < len(data); i++ {
		if data[i] == '\r' {
			count++
			tokens[*tokenIndex][0] = advance
			tokens[*tokenIndex][1] = i
			*tokenIndex++
			advance = i + 1
		}
	}

	counts.CR = int64(count)

	if atEOF {
		if checkEOF(data, tokens, tokenIndex, &advance, EmptyEOF) {
			counts.EmptyEOF = true
		}
	}

	return
}
