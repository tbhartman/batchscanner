package batchscanner

// Options to specify a LineSplitter
type Options struct {
	// AllowEndings specifies which endings to look for
	AllowEndings EOL

	// CheckMixed means to check that only one type of AllowEndings are present.
	CheckMixed bool

	// CheckAllowed means to check that only AllowEndings are present.
	CheckAllowed bool
}

// OptBufio specifies options similar to bufio.ScanLines
var OptBufio Options = Options{
	AllowEndings: LF | CRLF | EmptyEOF,
}

// OptParseAny scans lines for a file with any or mixed line endings
var OptParseAny Options = Options{
	AllowEndings: CR | LF | CRLF | EmptyEOF,
}

// OptWellFormed checks that a file is either LF or CRLF with ending newline.
var OptWellFormed Options = Options{
	AllowEndings: LF | CRLF,
	CheckAllowed: true,
	CheckMixed:   true,
}

// OptDefault scans lines for a file with any or mixed line endings
var OptDefault = OptParseAny
